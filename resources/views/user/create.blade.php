@extends('home')
@section('content')
    <div >
        <div id= "contentsigin">
            <div id = "form">
                <form action ="{{ route('users.store')}}" method ='post' class = 'text-center'>
                @csrf
                    <div class = 'form-group'>
                        <h1>Đăng ký tài khoản</h1>
                    </div>
                    <div class = "form-group {{$errors->has('mail_address') ? 'has-error' : ''}} ">
                        <lable for = 'mail_address' class="control-label" >địa chỉ email</lable><br/>
                        <input type = 'text' name = 'mail_address' id = 'mail_address' class="form-control" value="{{old('mail_address')}}">
                        @error('mail_address')
                            <div style='color:red' class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class = "form-group {{$errors->has('name') ? 'has-error' : ''}} "> 
                        <lable for = 'name' class="control-label">tên</lable><br/>
                        <input type = 'text' name = 'name' id = 'name' class="form-control" value="{{ old('name') }}">
                        @error('name')
                            <div style='color:red' class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class = "form-group {{$errors->has('password') ? 'has-error' : ''}} ">
                        <lable for='password' class="control-label">mật khẩu</lable><br/>
                        <input type = 'password' name = 'password' id = 'password' class="form-control" value="{{ old('password') }}">
                        @error('password')
                            <div style='color:red' class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>  
                    <div class = "form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}} ">
                        <lable for = 'password_confirmation'class="control-label">nhập lại mât khẩu</lable><br/>
                        <input type = 'password' name = 'password_confirmation' id = 'password_confirmation' class="form-control" value="{{ old('password_confirmation') }}">
                        @error('password_confirm')
                            <div style='color:red' class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class = "form-group {{$errors->has('address') ? 'has-error' : ''}} ">
                        <lable for = 'address' class="control-label">địa chỉ</lable><br/>
                        <input type = 'text' name = 'address' id = 'address' class="form-control" value="{{ old('address') }}">
                        @error('address')
                            <div style='color:red' class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class = "form-group {{$errors->has('phone') ? 'has-error' : ''}} ">
                        <lable for = 'phone' class="control-label">số điện thoại</lable><br/>
                        <input type = 'number' name = 'phone' id = 'phone' class="form-control" value="{{ old('phone') }}">
                        @error('phone')
                            <div style='color:red' class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class = "form-group">
                        <button type = 'submit' name = 'submit' class = 'btn btn-primary'>Tạo tài khoản</button> 
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection