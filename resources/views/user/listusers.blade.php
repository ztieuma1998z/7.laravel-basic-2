@extends('home')
@section('content')
    <div id ="content"> 
        <div id ="headerlist">
            <div>
                <div>
                    <h1>DANH SÁCH NGƯỜI DÙNG</h1>
                </div>
                <div style='color:green'>
                    @if(session()->has('message'))
                        <div>{{session('message')}}</div>
                    @endif
                </div>
            </div> 
            <div>
                <form action ="{{ route('users.create')}}" method ='get'>
                    <div>
                        <button type = 'submit' name = 'submit' class = 'btn btn-primary'>TẠO TÀI KHOẢN</button> 
                    </div>
                </form>
            </div>
        </div>
        <div id="table" > 
            <table>
                <thead>
                    <tr>
                        <th id = "headertable">STT</th>
                        <th id = "headertable">Địa chỉ email</th>
                        <th id = "headertable">Tên</th>
                        <th id = "headertable">Địa chỉ</th>
                        <th id = "headertable">Số điện thoại</th>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $serial = ($data->currentPage()-1)*($data->perPage());
                    @endphp
                    @foreach ($data as $key => $dt)
                        <tr id ="data">
                            <td>{{$serial + $key + 1}}</td>
                            <td>{{$dt->mail_address}}</td>
                            <td>{{$dt->name}}</td>
                            <td>{{$dt->address}}</td>
                            <td>{{$dt->phone}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div >
            <spam>
                {{ $data->links() }}
            </spam>
        </div>
    </div>
@endsection