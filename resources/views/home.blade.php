<html> 
<header>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<title>@yield('title')</title>
<style type="text/css">
    #header{
        background-color:#39e4c2;
        text-align:center;
        padding:5px 0px;
        font-size: 17pt;
        height: 120px;
    }
    #content{
        padding:10px 20px;
        height:  calc(100vh - 240px);
        text-align: center;
    }
    #data {
        padding: 5px 0px;
        height: 25px;
    }
    #headertable {
        text-align: center;
    }
    #form {
        width: 300px;
    }

    #contentsigin {
        display: flex;
        justify-content: center;
        width: 100%; 
    }

    #headerlist{
        display: flex;
        justify-content:space-between;
        align-items: center;
    }
    #table {
        display: flex;
        justify-content: center;
    }
    </style>
</header>
<body>
    <div id="header" >
        <h2>Monstar Lab 2021</h2>
        <p>laravel basic 2</p>
    </div>
    <div >
        @yield('content')
    </div>
</body>