<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * function index().
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.list');
    }
}
