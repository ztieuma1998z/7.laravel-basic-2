<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Models\User;

class UsersController extends Controller
{   
    public $user;

    /**
     * create construct oject 
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = $this->user->getUsers(); 
        return view('user.listusers', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  pp\Http\Requests\CreateUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {   
        $this->user->createUser($request);
        session()->flash('message', 'Thêm mới người dùng thành công');
        return redirect()->route('users.index');
    }
}
