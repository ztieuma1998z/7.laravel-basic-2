<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use HasFactory, softDeletes;
    protected $perPage = 20;
    protected $fillable = ['mail_address','password', 'name', 'address', 'phone'];
    /**
     * Get all Users .
     *
     * @return oject $data
     */
    public function getUsers ()
    {
        return $data = user::orderBy('mail_address')->paginate();
    }

     /**
     * create Users .
     * @param oject $request
     * @return oject session()
     */
    public function createUser($request)
    {   
        $input = $request->validated();
        $input['password'] = bcrypt($input['password']);
        $add = user::create($input);
    }
}
